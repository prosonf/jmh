package challenges;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

@State(value = Scope.Benchmark)
public class SoupBenchmark {

    private static final String ALPHABET_NO_Z = "abcdefghijklmnopqrstuvwy";

    String message;
    String bowlSoupContent;

    @Setup
    public void setup() {
        message = "messagenotcontainedz";
        StringBuilder stringBuilder = new StringBuilder(ALPHABET_NO_Z);
        IntStream.range(0, 100).forEach(i -> stringBuilder.append(ALPHABET_NO_Z));
        bowlSoupContent = stringBuilder.toString();
    }

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(SoupBenchmark.class.getSimpleName())
                .warmupIterations(2)
                .measurementIterations(4)
                .forks(1)
                .build();
        new Runner(opt).run();
    }

    @Benchmark
    public boolean canIWriteMyMessageDavid() {

        Map<Character, Long> messageContent = message.chars().parallel()
                .mapToObj(c -> (char) c)
                .filter(Character::isAlphabetic)
                .collect(groupingBy(Function.identity(), counting()));

        Map<Character, Long> soupContent = bowlSoupContent.chars().parallel()
                .mapToObj(c -> (char) c)
                .filter(Character::isAlphabetic)
                .collect(groupingBy(Function.<Character>identity(), counting()));

        return messageContent
                .keySet()
                .stream()
                .allMatch(current -> messageContent.get(current) <= soupContent.getOrDefault(current, Long.valueOf(0)));
    }

    @Benchmark
    public boolean canIWriteMyMessage() {

        LetterClassifier letterClassifier = classifyLetters(message);

        // Stream soup letters
        // -> filter contained on message
        // -> map decreasing counter
        // -> stop condition limiting by message size
        // -> terminal op
        bowlSoupContent.chars()
                .mapToObj(c -> String.valueOf((char) c))
                .filter(letterClassifier::containsLetter)
                .map(letterClassifier::discountLetter)
                .limit(letterClassifier.count)
                .count();

        return letterClassifier.areAllLettersDiscounted();
    }

    protected LetterClassifier classifyLetters(String message) {
        LetterClassifier letterClassifier = new LetterClassifier();
        message.chars()
                .filter(Character::isAlphabetic)
                .mapToObj(c -> Character.toString((char) c))
                .forEach(letterClassifier);
        return letterClassifier;
    }

    static class LetterClassifier implements Consumer<String> {
        Map<String, Long> messageLettersMap = new HashMap<>();
        int count = 0;

        @Override
        public void accept(String s) {
            Long currentCount = messageLettersMap.containsKey(s) ? messageLettersMap.get(s) : 0L;
            messageLettersMap.put(s, currentCount + 1);
            count++;
        }

        public boolean containsLetter(String letter) {
            return messageLettersMap.containsKey(letter);
        }

        public String discountLetter(String letter) {
            Long currentCount = messageLettersMap.get(letter);
            if (currentCount > 1) {
                messageLettersMap.put(letter, currentCount - 1);
            } else {
                messageLettersMap.remove(letter);
            }
            return letter;
        }

        public boolean areAllLettersDiscounted() {
            return messageLettersMap.isEmpty();
        }
    }

    @Benchmark
    public boolean canIWriteMyMessageMixedApproach1NotConvertingToString() {
        LetterClassifierMixedApproach letterClassifier = new LetterClassifierMixedApproach();
        message.chars()
                .filter(Character::isAlphabetic)
                .mapToObj(c -> (char) c)
//                .mapToObj(c -> Character.toString((char) c))
                .forEach(letterClassifier);

        // Stream soup letters
        // -> filter contained on message
        // -> map decreasing counter
        // -> stop condition limiting by message size
        // -> terminal op
        bowlSoupContent.chars()
//                .mapToObj(c -> String.valueOf((char) c))
                .mapToObj(c -> (char) c)
                .filter(letterClassifier::containsLetter)
                .map(letterClassifier::discountLetter)
                .limit(letterClassifier.count)
                .count();

        return letterClassifier.areAllLettersDiscounted();
    }

    @Benchmark
    public boolean canIWriteMyMessageMixedApproach2AndCollectingWithGrouping() {

        LetterClassifierMixedApproach letterClassifier = new LetterClassifierMixedApproach();
        Map<Character, Long> messageClassified = message.chars()
                .mapToObj(c -> (char) c)
                .filter(Character::isAlphabetic)
                .collect(groupingBy(Function.identity(), counting()));
        letterClassifier.messageLettersMap = messageClassified;

        // Stream soup letters
        // -> filter contained on message
        // -> map decreasing counter
        // -> stop condition limiting by message size
        // -> terminal op
        bowlSoupContent.chars()
//                .mapToObj(c -> String.valueOf((char) c))
                .mapToObj(c -> (char) c)
                .filter(letterClassifier::containsLetter)
                .map(letterClassifier::discountLetter)
                .limit(letterClassifier.count)
                .count();

        return letterClassifier.areAllLettersDiscounted();
    }

    @Benchmark
    public boolean canIWriteMyMessageMixedApproach3AndWithOneParallel() {

        LetterClassifierMixedApproach letterClassifier = new LetterClassifierMixedApproach();
        Map<Character, Long> messageClassified = message.chars().parallel()
                .mapToObj(c -> (char) c)
                .filter(Character::isAlphabetic)
                .collect(groupingBy(Function.identity(), counting()));
        letterClassifier.messageLettersMap = messageClassified;

        // Stream soup letters
        // -> filter contained on message
        // -> map decreasing counter
        // -> stop condition limiting by message size
        // -> terminal op
        bowlSoupContent.chars()
//                .mapToObj(c -> String.valueOf((char) c))
                .mapToObj(c -> (char) c)
                .filter(letterClassifier::containsLetter)
                .map(letterClassifier::discountLetter)
                .limit(letterClassifier.count)
                .count();

        return letterClassifier.areAllLettersDiscounted();
    }

    static class LetterClassifierMixedApproach implements Consumer<Character> {
        Map<Character, Long> messageLettersMap = new HashMap<>();
        int count = 0;

        @Override
        public void accept(Character s) {
            Long currentCount = messageLettersMap.containsKey(s) ? messageLettersMap.get(s) : 0L;
            messageLettersMap.put(s, currentCount + 1);
            count++;
        }

        public boolean containsLetter(Character letter) {
            return messageLettersMap.containsKey(letter);
        }

        public Character discountLetter(Character letter) {
            Long currentCount = messageLettersMap.get(letter);
            if (currentCount > 1) {
                messageLettersMap.put(letter, currentCount - 1);
            } else {
                messageLettersMap.remove(letter);
            }
            return letter;
        }

        public boolean areAllLettersDiscounted() {
            return messageLettersMap.isEmpty();
        }
    }

}
